@extends('layouts.template')

@section('content')

<script src="assets/js/dropzone.js"></script>
<script>
    Dropzone.options.addPhotoForm = {
        paramName: 'photo',
        maxFilesize:3,
        acceptedFiles: '.jpg, .jpeg, .png, .bmp',
    }
</script>

<div class="panel panel-default">
    <div class="panel-heading" style="height: 70px">
        <div class="left pannel-heading" style="float: left;">
            <div class="page-title">
                <h4>Upload Photos</h4>
            </div>
        </div>
    </div>

    <div class="panel-body">
        <div class="members-pannel">
            <div class="row">
                <div class="col-md-8 col-lg-offset-2">
                        <form id="addPhotoForm" method="post" action="users/photos" class="dropzone">
                            {{ csrf_field() }}
                            <div class="fallback">
                                <input name="file" type="file" multiple />
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    <script src="assets/js/dropzone.js"></script>
    <script>
        Dropzone.options.addPhotoForm = {
            paramName: 'photo',
            maxFilesize:3,
            acceptedFiles: '.jpg, .jpeg, .png, .bmp',
        }
    </script>
@endsection