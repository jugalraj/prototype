@extends('layouts.template')

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#users">Users</a></li>
        <li><a data-toggle="tab" href="#groups">Groups</a></li>
        <li><a data-toggle="tab" href="#roles">Roles</a></li>
        <li><a data-toggle="tab" href="#permissions">Permissions</a></li>
    </ul>

    <div class="tab-content">
        <div id="users" class="tab-pane fade in active">
            <div class="panel panel-default">
                <div class="panel-heading" style="height: 70px">
                    <div class="left pannel-heading" style="float: left;">
                        <div class="page-title">
                            <h4>MMS Users</h4>
                        </div>
                    </div>
                    <div class="right_pannel-heading" style="float: right">
                        {!! Form::open(['method' => 'GET', 'action' => ['UsersController@index'], 'class'=> "navbar-form navbar-left"]) !!}
                        <div class="form-group">
                            <span class="icon"><i class="fa fa-search"></i></span>
                            {!! Form::input('search','search',null,['id'=> 'searchBox', 'class'=>'form-control', 'placeholder'=>'Search...']) !!}
                        </div>
                        <input type="submit" hidden/>
                        {!! Form::close() !!}
                        <a href="/users/create"><img src="/assets/img/add_user.png" height="35" width="35"/></a>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="membes-pannel">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Full Name</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Group</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td><input type="checkbox"/></td>
                                            <td><a href="{{ url('users',$user->id) }}"> {{ $user->full_name }}</a></td>
                                            <td>{{ $user->username }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ ucfirst(implode(', ',$user->getGroups())) }}</td>
                                            <td>{{ $user->status }}</td>
                                            <td>
                                                {!! Form::open(['route' => ['users.destroy', $user   ->id], 'method' => 'delete']) !!}
                                                {{--<button class="btn-success" href="/users/{!! $user->id !!}/edit">Edit</button>--}}
                                                <a href="/users/{!! $user->id !!}/edit" class="btn btn-success">Edit</a>
                                                <button class="btn btn-danger" type="submit">Delete</button>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="groups" class="tab-pane fade">
            <div class="panel panel-default">
                <div class="panel-heading" style="height: 70px">
                    <div class="left pannel-heading" style="float: left;">
                        <div class="page-title">
                            <h4>MMS Groups</h4>
                        </div>
                    </div>
                    <div class="right_pannel-heading" style="float: right">
                        {!! Form::open(['method' => 'GET', 'action' => ['UsersController@index'], 'class'=> "navbar-form navbar-left"]) !!}
                        <div class="form-group">
                            <span class="icon"><i class="fa fa-search"></i></span>
                            {!! Form::input('search','search',null,['id'=> 'searchBox', 'class'=>'form-control', 'placeholder'=>'Search...']) !!}
                        </div>
                        <input type="submit" hidden/>
                        {!! Form::close() !!}
                        <a href="/users/create"><img src="/assets/img/add_user.png" height="35" width="35"/></a>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="membes-pannel">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Group Name</th>
                                        <th>Roles</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{--@foreach($groups as $group)--}}
                                        {{--<tr>--}}
                                            {{--<td><input type="checkbox"/></td>--}}
                                            {{--<td><a href="{{ $group->name }}</a></td>--}}
                                            {{--<td>{{ $user->username }}</td>--}}
                                            {{--<td>{{ $user->email }}</td>--}}
                                            {{--<td>{{ ucfirst(implode(', ',$user->getGroups())) }}</td>--}}
                                            {{--<td>{{ $user->status }}</td>--}}
                                            {{--<td>--}}
                                                {{--{!! Form::open(['route' => ['users.destroy', $user   ->id], 'method' => 'delete']) !!}--}}
                                                {{--<button class="btn-success" href="/users/{!! $user->id !!}/edit">Edit</button>--}}
                                                {{--<a href="/users/{!! $user->id !!}/edit" class="btn btn-success">Edit</a>--}}
                                                {{--<button class="btn btn-danger" type="submit">Delete</button>--}}
                                                {{--{!! Form::close() !!}--}}
                                            {{--</td>--}}
                                        {{--</tr>--}}
                                    {{--@endforeach--}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection