@inject('groups','\App\group')

        <!-- Full Name Form Text Input -->
<div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
    {!! Form::label('full_name','Full Name ',['class' =>'col-md-4 control-label']) !!}

    <div class="col-md-6">
        {!! Form::text('full_name',null,['class'=>'form-control']) !!}

        @if ($errors->has('full_name'))
            <span class="help-block">
                <strong>{{ $errors->first('full_name') }}</strong>
            </span>
        @endif
    </div>
</div>

<!-- Username Form Text Input -->
<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
    {!! Form::label('username','Username ',['class' =>'col-md-4 control-label']) !!}

    <div class="col-md-6">
        {!! Form::text('username',null,['class'=>'form-control']) !!}

        @if ($errors->has('username'))
            <span class="help-block">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">E-Mail Address</label>

    <div class="col-md-6">
        <input type="email" class="form-control" name="email" value="{{ old('email') }}">

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Password</label>

    <div class="col-md-6">
        <input type="password" class="form-control" name="password">

        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Confirm Password</label>

    <div class="col-md-6">
        <input type="password" class="form-control" name="password_confirmation">

        @if ($errors->has('password_confirmation'))
            <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('groups') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">User Groups</label>

    <div class="col-md-6">
        {!! Form::select('groups[]',$groups,null,['class' =>'form-control','multiple']) !!}

        @if ($errors->has('groups'))
            <span class="help-block">
                <strong>{{ $errors->first('groups') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-btn fa-user"></i>Add User
        </button>
    </div>
</div>
