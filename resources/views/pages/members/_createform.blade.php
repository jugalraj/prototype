<!-- Full Name Form Text Input -->
<div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
    {!! Form::label('','Full Name ',['class' =>'col-md-2 control-label']) !!}

    <div class="col-md-4">
        {!! Form::text('full_name',null,['class'=>'form-control']) !!}

        @if ($errors->has('full_name'))
            <span class="help-block">
                <strong>{{ $errors->first('full_name') }}</strong>
            </span>
        @endif
    </div>
</div>

<!-- Date of Birth Form Text Input -->
<!-- Date of Birth Form Text Input -->
<div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
    {!! Form::label('dob','Date of Birth ',['class' =>'col-md-2 control-label']) !!}

    <div class="col-md-4">
        {!! Form::input('date','dob',null,['class'=>'form-control']) !!}

        @if ($errors->has('dob'))
            <span class="help-block">
                <strong>{{ $errors->first('dob') }}</strong>
            </span>
        @endif
    </div>
</div>


<!-- Citizenship No. Form Text Input -->
<div class="form-group{{ $errors->has('citizenship_no') ? ' has-error' : '' }}">
    {!! Form::label('citizenship_no','Citizenship No. ',['class' =>'col-md-2 control-label']) !!}

    <div class="col-md-4">
        {!! Form::text('citizenship_no',null,['class'=>'form-control']) !!}

        @if ($errors->has('citizenship_no'))
            <span class="help-block">
                <strong>{{ $errors->first('citizenship_no') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--Gender Form Radio Input --}}
<div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
    {!! Form::label('gender','Gender ',['class' =>'col-md-2 control-label']) !!}

    <div class="col-md-4">
        <div class="radio">
            <label>
                <input name="gender" value="Male" id="gender" type="radio" {{ $member->gender == 'Male' ? 'checked' : '' }} /> Male
            </label>
            <label>
                <input name="gender" value="Feale" id="gender" type="radio" {{ $member->gender == 'Female' ? 'checked' : '' }} /> Female
            </label>
            <label>
                <input name="gender" value="3rd Gender" id="gender" type="radio" {{ $member->gender == '3rd Gender' ? 'checked' : '' }}> 3rd Gender
            </label>
        </div>

         @if ($errors->has('gender'))
            <span class="help-block">
                <strong>{{ $errors->first('gender') }}</strong>
            </span>
        @endif
    </div>
</div>

{{-- Permanent Address Section --}}
<div class="form-group">
    {!! Form::label('permanent_address','Permanent Address ',['class' =>'col-md-2 control-label']) !!}
</div>

<!-- Permanent District Form Text Input -->
<div class="form-group{{ $errors->has('perm_address') ? ' has-error' : '' }}">
    <tr>
        <td>
            <!-- District Form Text Input -->
                {!! Form::label('perm_district','District ',['class' =>'col-md-2 control-label']) !!}
            
                <div class="col-md-2">
                    <select class="form-control" name="perm_district">
                        @foreach($districts as $value)
                            <option value="{{ $value }}">{{ $value }}</option>
                        @endforeach
                    </select>
            
                    @if ($errors->has('perm_district'))
                        <span class="help-block">
                            <strong>{{ $errors->first('perm_district') }}</strong>
                        </span>
                    @endif
                </div>
        </td>
        <td>
            <!-- VDC Form Text Input -->
            <!-- VDC Form Text Input -->
                {!! Form::label('perm_vdc','VDC ',['class' =>'col-md-2 control-label']) !!}

                <div class="col-md-2">
                    <select class="form-control" name="perm_vdc">
                        @foreach($vdcs as $value)
                            <option value="{{ $value }}">{{ $value }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('perm_vdc'))
                        <span class="help-block">
                            <strong>{{ $errors->first('perm_vdc') }}</strong>
                        </span>
                    @endif
                </div>
        </td>
        <td>
            <!-- Ward Form Text Input -->
                {!! Form::label('perm_ward','Ward ',['class' =>'col-md-2 control-label']) !!}
            
                <div class="col-md-1">
                    {!! Form::selectRange('perm_ward', 1, 35, null, ['class'=>'form-control']) !!}
            
                    @if ($errors->has('perm_ward'))
                        <span class="help-block">
                            <strong>{{ $errors->first('perm_ward') }}</strong>
                        </span>
                    @endif
                </div>
        </td>
</div>

{{-- Current Address Section --}}
<div class="form-group">
    {!! Form::label('current_address','Current Address ',['class' =>'col-md-2 control-label']) !!}
</div>

<!-- Country Form Text Input -->
<div class="form-group{{ $errors->has('curr_country') ? ' has-error' : '' }}">
    {!! Form::label('curr_country','Country ',['class' =>'col-md-2 control-label']) !!}

    <div class="col-md-2">
        <select class="form-control" name="curr_country">
            @foreach($districts as $value)
                <option value="{{ $value }}">{{ $value }}</option>
            @endforeach
        </select>

        @if ($errors->has('country'))
            <span class="help-block">
                <strong>{{ $errors->first('country') }}</strong>
            </span>
        @endif
    </div>
</div>


<!-- Permanent District Form Text Input -->
<div class="form-group{{ $errors->has('curr_address') ? ' has-error' : '' }}">
    <tr>
        <td>
            <!-- District Form Text Input -->
                {!! Form::label('curr_district','District ',['class' =>'col-md-2 control-label']) !!}

                <div class="col-md-2">
                    <select class="form-control" name="curr_district">
                        @foreach($districts as $value)
                            <option value="{{ $value }}">{{ $value }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('curr_district'))
                        <span class="help-block">
                            <strong>{{ $errors->first('curr_district') }}</strong>
                        </span>
                    @endif
                </div>
        </td>
        <td>
            <!-- VDC Form Text Input -->
                {!! Form::label('curr_vdc','VDC ',['class' =>'col-md-2 control-label']) !!}

                <div class="col-md-2">
                    <select class="form-control" name="curr_vdc">
                        @foreach($vdcs as $value)
                            <option value="{{ $value }}">{{ $value }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('curr_vdc'))
                        <span class="help-block">
                            <strong>{{ $errors->first('curr_vdc') }}</strong>
                        </span>
                    @endif
                </div>
        </td>
        <td>
            <!-- Ward Form Text Input -->
                {!! Form::label('curr_ward','Ward ',['class' =>'col-md-2 control-label']) !!}

                <div class="col-md-1">
                    {!! Form::selectRange('curr_ward', 1, 35, null, ['class'=>'form-control']) !!}

                    @if ($errors->has('curr_ward'))
                        <span class="help-block">
                            <strong>{{ $errors->first('curr_ward') }}</strong>
                        </span>
                    @endif
                </div>
        </td>
    </tr>
</div>

<!-- Phone No Form Text Input -->
<div class="form-group{{ $errors->has('contact_no') ? ' has-error' : '' }}">
    {!! Form::label('contact_no','Phone No ',['class' =>'col-md-2 control-label']) !!}

    <div class="col-md-2">
        {!! Form::input('phone','contact_no',null,['class'=>'form-control']) !!}

        @if ($errors->has('contact_no'))
            <span class="help-block">
                <strong>{{ $errors->first('contact_no') }}</strong>
            </span>
        @endif
    </div>
</div>

<!-- Email Form Text Input -->
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    {!! Form::label('email','Email ',['class' =>'col-md-2 control-label']) !!}

    <div class="col-md-3">
        {!! Form::input('email','email',null,['class'=>'form-control']) !!}

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>

<!-- Skype Form Text Input -->
<div class="form-group{{ $errors->has('skype') ? ' has-error' : '' }}">
    {!! Form::label('skype','Skype ',['class' =>'col-md-2 control-label']) !!}

    <div class="col-md-3">
        {!! Form::text('skype',null,['class'=>'form-control']) !!}

        @if ($errors->has('skype'))
            <span class="help-block">
                <strong>{{ $errors->first('skype') }}</strong>
            </span>
        @endif
    </div>
</div>

<!-- Qualification Form Text Input -->
<div class="form-group{{ $errors->has('qualification') ? ' has-error' : '' }}">
    <tr>
        <td>
            <!-- Qualification Form Text Input -->
                {!! Form::label('qualification','Qualification ',['class' =>'col-md-2 control-label']) !!}

                <div class="col-md-3">
                    {!! Form::select('qualification' ,[
                        '' =>'',
                        'Under SLC' => 'Under SLC',
                        'SLC'   => 'SLC',
                        'Ten +2' => 'Ten +2',
                        'Bachelor' => 'Bachelor',
                        'Master' => 'Master',
                        'Doctorate' => 'Doctorate',
                        'No Formal Education' => 'No Formal Education'
                    ], null, ['class'=>'form-control']) !!}

                    @if ($errors->has('qualification'))
                        <span class="help-block">
                            <strong>{{ $errors->first('qualification') }}</strong>
                        </span>
                    @endif
                </div>
        </td>
        <td>
            <!-- Specialization Form Text Input -->
            <!-- Specialization Form Text Input -->
                {!! Form::label('specialization','Specialization ',['class' =>'col-md-2 control-label']) !!}

                <div class="col-md-3">
                    {!! Form::text('specialization',null,['class'=>'form-control']) !!}

                    @if ($errors->has('specialization'))
                        <span class="help-block">
                                <strong>{{ $errors->first('specialization') }}</strong>
                            </span>
                    @endif
                </div>
        </td>
    </tr>
</div>

<div class="form-group{{ $errors->has('kind_of_help') ? ' has-error' : '' }}">
    {!! Form::label('kind_of_help','Support By ',['class' =>'col-md-2 control-label']) !!}

    <div class="col-md-4">
        <div class="checkbox">
            <label>
                <input name="kind_of_help" value="Time" type="checkbox" {{ ($member->kind_of_help == 'Time')?'checked="checked':'' }} /> Time
            </label>
            <label>
                <input name="kind_of_help" value="Network" type="checkbox" {{ ($member->kind_of_help == 'Network')?'checked=checked':'' }}> Network
            </label>
            <label>
                <input name="kind_of_help" value="Fund/Resource" type="checkbox" {{ ($member->kind_of_help == 'Fund/Resource')?'checked=checked':'' }}> Fund/Resource
            </label>
        </div>

         @if ($errors->has('kind_of_help'))
            <span class="help-block">
                <strong>{{ $errors->first('kind_of_help') }}</strong>
            </span>
        @endif
    </div>
</div>
<hr/>

<div class="form-group">
    <div class="col-md-offset-5">
        <button type="reset" class="btn btn-primary">
            <i class="fa fa-btn fa-sign-in"></i> Reset
        </button>
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-btn fa-sign-in"></i> {{ $submitText }}
        </button>
    </div>
</div>