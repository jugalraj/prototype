@extends('layouts.template')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading" style="height: 70px">
            <div class="left pannel-heading" style="float: left;">
                <div class="page-title">
                    <h4>Members</h4>
                </div>
            </div>
            <div class="right_pannel-heading" style="float: right">
                {!! Form::open(['method' => 'GET', 'action' => ['MembersController@index'], 'class'=> "navbar-form navbar-left"]) !!}
                    <div class="form-group">
                        <span class="icon"><i class="fa fa-search"></i></span>
                        {!! Form::input('search','search',null,['id'=> 'searchBox', 'class'=>'form-control', 'placeholder'=>'Search...']) !!}
                    </div>
                   <input type="submit" hidden/>
                {!! Form::close() !!}
                <a href="/members/create"><img src="/assets/img/add_user.png" height="35" width="35"/></a>
                <a href="/members/destroy/"><img src="/assets/img/delete_user.png" height="35" width="35"/></a>
            </div>
        </div>

        <div class="panel-body">
            <div class="membes-pannel">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Full Name</th>
                                <th>Current Address</th>
                                <th>Permanent Address</th>
                                <th>Contact No</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($members as $member)
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td><a href="{{ url('members',$member->id) }}"> {{ $member->full_name }}</a></td>
                                        <td>{{ $member->curr_vdc.' '.$member->curr_ward.' ,'.$member->curr_district }}</td>
                                        <td>{{ $member->perm_vdc.' '.$member->perm_ward.' ,'.$member->perm_district }}</td>
                                        <td>{{ $member->contact_no }}</td>
                                        <td>Active</td>
                                        <td>
                                            <a href="/members/{!! $member->id !!}/edit"><img src="/assets/img/edit_user.png" height="25" width="25" /></a>
                                            {{--<a href="{!! URL::route('members.destroy',$member->id) !!}"></a>--}}
                                            <script src="https://gist.githubusercontent.com/soufianeEL/3f8483f0f3dc9e3ec5d9/raw/625737f6960776d344a8780265047f608f5153cf/laravel.js" ></script>
                                            <a href="{!! URL::route('members.destroy',$member->id) !!}" data-method="delete" data-token="{{ csrf_token() }}" data-confirm="Are you sure?">
                                                <img src="/assets/img/delete_user.png" height="25" width="25" />
                                            </a>
                                            {{--{!! Form::open(['method' => 'DELETE' ,'action' => ['MembersController@destroy',$member->id], 'class' => 'form-horizontal']) !!}--}}
                                                {{--<button class="btn" type="submit">--}}
                                                    {{--<img src="/assets/img/delete_user.png" height="35" width="25">--}}
                                                {{--</button>--}}
                                            {{--{!! Form::close() !!}--}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection