<table class="table">
    <tbody>
        <tr>
            <td><label>Citizenship No: </label></td>
            <td>{{ $member->citizenship_no }}</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><label>Date of Birth: </label></td>
            <td>{{ $member->dob }}</td>
            <td><label>Age: </label></td>
            <td></td>
        </tr>
        <tr>
            <td><label>Gender: </label></td>
            <td>{{ $member->gender }}</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><label>Permanent Address: </label></td>
            <td>{{ $member->perm_vdc.' '.$member->perm_ward.' ,'.$member->perm_district }}</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><label>Current Address: </label></td>
            <td>{{ $member->curr_vdc.' '.$member->curr_ward.' ,'.$member->curr_district }}</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><label>Phone no: </label></td>
            <td>{{ $member->contact_no }}</td>
            <td><label>Skype ID: </label></td>
            <td>{{ $member->skype }}</td>
        </tr>
        <tr>
            <td><label>Email ID: </label></td>
            <td>{{ $member->email }}</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><label>Qualification: </label></td>
            <td>{{ $member->qualification}} {{$member->specialization?'('. $member->specialization .')':' ' }}</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><label>Help Bibeksheel by: </label></td>
            <td>{{ $member->kind_of_help }}</td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>
