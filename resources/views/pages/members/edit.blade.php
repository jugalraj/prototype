@extends('layouts.template')

@section('content')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="left pannel-heading">
                    <div class="page-title">
                        <h4>
                            <img class="icon-bar" src="/assets/img/add_user.png" height="25" width="25"></image>
                            Edit Member
                        </h4>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                {{--{{ dd($member) }}--}}
                {!! Form::model($member, ['method' => 'PUT', 'action' => ['MembersController@update',$member->id], 'class' => 'form-horizontal']) !!}
                    @include('pages.members._createform')
                {!! Form::close() !!}
            </div>
        </div>


    </div>


@endsection