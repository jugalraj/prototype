@extends('layouts.template')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="left pannel-heading"><h4>Member Details: {{ $member->full_name }}</h4></div>
                    </div>

                    <div class="panel-body">
                        <div class="members-pannel col-md-10">
                            @include('pages.members._member_details')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
