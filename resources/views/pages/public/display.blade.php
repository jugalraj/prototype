<div class="panel panel-default">
    <div class="panel-heading" style="height: 70px">
        <div class="left pannel-heading" style="float: left;">
            <div class="page-title">
                <h4>Publicly Available Photos</h4>
            </div>
        </div>
    </div>

    <ul class="row">
        @foreach($photos as $photo)
            <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
                <a class="" href="{{'photos/'.$photo->name}}" data-lity>
                    <img class="img-responsive" src="photos/{{ $photo->thumb_name }}" alt=""/>
                </a>
            </li>
        @endforeach
    </ul>
</div>
