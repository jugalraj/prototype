@inject('user','App\User')
@extends('layouts.template')

@section('content')
    @if(Auth::guest())
        <!-- Main component for a primary marketing message or call to action -->
        <div class="container">
            @include('pages.public.display');
        </div>
    @else
        <div class="row">
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Private Wall</h3>
                    </div>
                    <div class="col-md-9">
                        @foreach($photos as $photo)
                            <img class="thumbBox" src="photos/{{ $photo->thumb_name }}" alt=""/>
                        @endforeach
                    </div>
                </div>
            </div><!-- /.col-sm-4 -->
        </div>
    @endif
@endsection

@section('footer-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lity/1.6.0/lity.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js" ></script>
@endsection