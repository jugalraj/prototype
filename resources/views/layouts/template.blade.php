<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Styles -->
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <link href="/assets/css/thumbnail-gallery.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/dropzone.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lity/1.6.0/lity.css">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}


</head>
<body id="app-layout">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Photo Site</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                @if (!Auth::guest())
                    <ul class="nav navbar-nav">
                            @can('manage_users')
                                <li><a href="/users">Users</a></li>
                            @endcan
                            @can('manage_members')
                            <li><a href="/members">Members</a></li>
                            @endcan
                            <li><a href="#contact">Contact</a></li>
                    </ul>
                @endif

                <!--/.nav-collapse -->

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    <li><a href="{{ url('upload') }}">Upload</a></li>
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('') }}"></a></li>
                        <li><a href="{{ url('') }}"></a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->full_name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    <div class="container theme-showcase" role="main">
        @yield('content')

        <!-- JavaScripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="https://raw.githubusercontent.com/enyo/dropzone/master/dist/dropzone.js" ></script>
        {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
        @yield('footer-script')
    </div>
</body>
</html>
