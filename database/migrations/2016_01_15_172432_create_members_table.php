<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name', 50);
            $table->string('citizenship_no',30);
            $table->string('gender',10);
            $table->date('dob');
            $table->string('perm_district',40);
            $table->string('perm_vdc',40);
            $table->string('perm_ward',40);
            $table->string('curr_country');
            $table->string('curr_district',40)->nullable();
            $table->string('curr_vdc',40)->nullable();
            $table->string('curr_ward',40)->nullable();
            $table->string('state',50)->nullable();
            $table->string('zip_or_postal',10)->nullable();
            $table->unsignedInteger('contact_no')->length(10)->unique();
            $table->string('email',50)->unique();
            $table->string('skype',30);
            $table->string('qualification',30);
            $table->string('specialization',30);
            $table->string('kind_of_help');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('members');
    }
}
