<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abodes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('district');
            $table->string('vdc');
            $table->unsignedInteger('vdc_code');
            $table->unsignedInteger('ward')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('abodes');
    }
}
