<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotUsersRolesGroupsPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acl_group_user', function (Blueprint $table) {
            $table->unsignedInteger('group_id');

            $table->unsignedInteger('user_id');

            $table->foreign('group_id')
                ->references('id')
                ->on('acl_groups')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('acl_permission_role', function (Blueprint $table) {
            $table->unsignedInteger('permission_id');

            $table->unsignedInteger('role_id');

            $table->foreign('permission_id')
                ->references('id')
                ->on('acl_permissions')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('role_id')
                ->references('id')
                ->on('acl_roles')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('acl_group_role', function (Blueprint $table) {
            $table->unsignedInteger('group_id');

            $table->unsignedInteger('role_id');

            $table->foreign('group_id')
                ->references('id')
                ->on('acl_groups')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('role_id')
                ->references('id')
                ->on('acl_roles')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('acl_group_user');
        Schema::drop('acl_group_role');
        Schema::drop('acl_role_permission');
    }
}
