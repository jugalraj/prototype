<?php

namespace App;


trait RolesTrait
{
    /**
     * Function : permission
     * Function for retrieving associated permission with role
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'acl_permission_role');
    }

    /**
     * Function : Group
     * Function for retrieving mapping group of role
     */
    public function groups()
    {
        return $this->BelongsToMany(Group::class,'acl_group_role');
    }

    /**
     * Function : getPermissions
     * Function for returning permissions associated
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * Function : getRoleName
     * Function to get role name
     *
     * @return mixed
     */
    public function getRoleName()
    {
        return $this->name;
    }

    /**
     * Function : getRoleID
     * Function to get role ID
     */
    public function getRoleID()
    {
        return $this->id;
    }

    /**
     * Function : grantPermission
     * Function for granting permission to role
     * @param Permission $permission
     *
     * @return Permission Saved
     */
    public function grantPermission(Permission $permission)
    {
        return $this->permissions()->save($permission);
    }

    /**
     * Function : revokePermission
     * Function for revoking permissions from role
     * @param Permission $permission
     * @return
     */
    public function revokePermission(Permission $permission)
    {
        return $this->permission()->detach($permission);
    }
}