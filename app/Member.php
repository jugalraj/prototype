<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'full_name',
        'dob',
        'gender',
        'perm_district',
        'perm_vdc',
        'perm_ward',
        'curr_vdc',
        'curr_district',
        'curr_ward',
        'citizenship_no',
        'contact_no',
        'email',
        'skype',
        'qualification',
        'specialization',
        'kind_of_help'
    ];
}
