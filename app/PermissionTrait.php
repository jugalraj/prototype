<?php

namespace App;


trait PermissionTrait
{
    /**
     * Function : Roles
     * Function for retrieving all roles that holds specific permission
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class,'acl_permission_role');
    }

    /**
     * Function : getRoleName
     * Function to get role name
     *
     * @return mixed
     */
    public function getPermissionName()
    {
        return $this->name;
    }

}