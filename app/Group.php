<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use GroupsTrait;

    protected $table = 'acl_groups';
}
