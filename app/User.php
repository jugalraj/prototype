<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{

    use UsersTrait;

    /**
     * Function : photos
     * Function for relation with photos
     */
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name','username', 'email', 'password', 'role'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function savePhoto(Photo $photo)
    {
        $this->photos()->save($photo);
    }
}
