<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use PermissionTrait;

    protected $table = 'acl_permissions';
}
