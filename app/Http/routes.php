<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/', 'HomeController@index');

    Route::get('/home', 'HomeController@index');

    Route::get('/members/delete_selected', 'MembersController@delete_selected');

    Route::resource('members', 'MembersController');

    Route::resource('users', 'UsersController');

    Route::get('upload', function(){
        $user = App\User::first();
        return view('pages.users.upload',compact('user'));
    });

    Route::get('display','UsersController@showPhoto');

    Route::post('users/photos','UsersController@addPhoto');

    Route::get('/test', function(){
//        Auth::loginusingid(5);
//        Auth::logout();
        return view('test');
    });
});
