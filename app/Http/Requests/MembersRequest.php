<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MembersRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'full_name' => 'required'
            ,'dob' => 'required|date'
            ,'gender' => 'required'
            ,'citizenship_no' => 'required'
            ,'perm_district' => 'required'
            ,'perm_vdc' => 'required'
            ,'perm_ward' => 'required'
            ,'curr_district' => 'required'
            ,'curr_vdc' => 'required'
            ,'curr_ward' => 'required'
            ,'contact_no' => 'required'
            ,'email' => 'required'
            ,'skype' => 'required'
            ,'qualification' => 'required'
            ,'kind_of_help' => 'required'
        ];
    }
}
