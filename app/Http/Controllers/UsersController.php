<?php

namespace App\Http\Controllers;

use App\Photo;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UsersController extends Controller
{
    /**
     * Function : __constructor
     * Function for Constructor
     */
    public function __construct()
    {
        /* This will call for authentication before allowing access
         * over other this module.
         */
        $this->middleware('role:admin',['except'=>'addPhoto']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($search = $request->input('search')) {
            $users = User::where('full_name', 'LIKE', '%'.$search.'%')
                ->orWhere('username', 'LIKE', '%'.$search.'%')
                ->orWhere('email', 'LIKE', '%'.$search.'%')
                ->orWhere('id', 'LIKE', '%'.$search.'%')->simplePaginate(50);
        }
        else {
            /** @var TYPE_NAME $members */
            $users = User::with('groups')->simplepaginate(50);
        }

        return view('pages.users.index', compact('users','search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request);

        User::create($request->all())->assignGroup($request->all());

        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(User $user)
    {
        return view('pages.users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param User $user
     * @internal param int $id
     */
    public function edit($id)
    {
        $user = User::findorfail($id);
        $submitText = ' Update ';

        return view('pages.users.edit',compact('user','submitText'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, User $user)
    {
        $user->update($request->all());

        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param User $user
     * @internal param int $id
     */
    public function destroy($id)
    {
        User::destroy($id);

        return redirect('users');
    }

    /**
     * Function for adding Photo by Users
     * @param Request $request
     * @return string
     */
    public function addPhoto(Request $request)
    {
        $photo = Photo::fromFile($request->file('photo'))->upload();

        $user = User::find(Auth::user()->id);

        $user->savePhoto($photo);
    }
}