<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user())
        {
            $photos = User::find(Auth::user()->id)->photos;
            return view('home',compact('photos'));
        }
        else
        {
            $photos = Photo::where('status','=',1)->get();
            return view('home',compact('photos'));
        }
    }
}
