<?php

namespace App\Http\Controllers;

use App\Http\Requests\MembersRequest;
use App\Member;
use App\Abode;
use Illuminate\Http\Request;

use App\Http\Requests;

class MembersController extends Controller
{
    /**
     * Function : __constructor
     * Function for Constructor
     */
    public function __construct()
    {
        /* This will call for authentication before allowing access
         * over other this module.
         */
//        $this->middleware('role:HR');
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($search = $request->input('search')) {
            $members = Member::where('full_name', 'LIKE', '%'.$search.'%')
                ->orWhere('citizenship_no', 'LIKE', '%'.$search.'%')
                ->orWhere('contact_no', 'LIKE', '%'.$search.'%')
                ->orWhere('id', 'LIKE', '%'.$search.'%')->simplePaginate(50);
        }
        else {
            $members = Member::SimplePaginate(50);
        }

        return view('pages.members.index', compact('members','search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $submitText = ' Add ';
        $districts = Abode::distinct()->lists('district');
        $vdcs = Abode::lists('vdc');

        return view('pages.members.create',compact('districts','vdcs','submitText'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param MembersRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(MembersRequest $request)
    {
        Member::create($request->all());

        return redirect('members');
    }

    /**
     * Display the specified resource.
     *
     * @param Member $member
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Member $member)
    {
        return view('pages.members.show',compact('member'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Member $member
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(Member $member)
    {
        $submitText = ' Update ';
        $districts = Abode::distinct()->lists('district');
        $vdcs = Abode::lists('vdc');

        return view('pages.members.edit',compact('member','districts','vdcs','submitText'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MembersRequest $request
     * @param Member $member
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(MembersRequest $request, Member $member)
    {
        $member->update($request->all());

        return redirect('members');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Member $member
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Member $member)
    {
        $member->delete();

        return redirect('members');
    }
}
