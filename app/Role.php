<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use RolesTrait;

    protected $table = 'acl_roles';
}
