<?php

namespace App;


trait GroupsTrait
{
    /**
     * Function : roles
     * Groups can have one role associated
     *
     * @return bool
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class,'acl_group_role');
    }

    /**
     * Function: Users
     * Groups can have many users
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'acl_group_user','group_id','user_id');
    }

    public function checkRole($role) {
        return !! $role->intersect($this->roles)->count();
    }


    /**
     * Function : getGroupName
     * Function to get group name
     *
     * @return mixed
     */
    public function getGroupName()
    {
        return $this->name;
    }

    /**
     * Function : getRoleID
     * Function to get group ID
     */
    public function getGroupID()
    {
        return $this->id;
    }

    /**
     * Function : assignRole
     * Function for assigning groups to role
     *
     * @param Role $role
     * @return Model
     */
    public function assignRole(Role $role)
    {
        return $this->roles()->save($role);
    }

    /**
     * Function : groupHasRole
     * Function to check if group has roles
     *
     * @param $name
     * @return bool
     * @internal param $id
     */
    public function groupHasRole($name)
    {
        $roles = $this->getRoles();
        foreach($roles as $role) {
            if($name == $role->name) {
                return true;
            }

            return false;
        }
    }

    /**
     * Function : getRoles
     * Function to get all associated roles
     *
     * @return mixed
     */
    public function getRoles()
    {
        $roles = $this->roles;
        foreach($roles as $role) {
            $allRoles[] = $role;
        }

        return $allRoles;
    }
}