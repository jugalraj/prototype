<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Photo extends Model
{

    protected $table='user_photos';
    protected $baseDir = 'photos';

    protected $file;

    protected $fillable = [
        'name',
        'size',
        'thumb_name',
    ];

    /**
     * @param UploadedFile $file
     * @return mixed
     */
    public static function fromFile(UploadedFile $file)
    {
        $photo = new static;

        $photo->file = $file;

        return $photo->fill([
            'name' => $photo->fileName(),
            'size' => $photo->fileSize(),
            'thumb_name' => $photo->thumbName(),
        ]);

    }

    public function fileName()
    {
        $name = sha1(
            $this->file->getClientOriginalName()
        );

        $extension = $this->file->getClientOriginalExtension();

        return "{$name}.{$extension}";
    }

    public function fileSize()
    {
        return $this->file->getMaxFilesize();
    }

    public function thumbName()
    {
        return "tn-".$this->fileName();
    }

    /**
     * Function : User
     * Function for relation with user
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Moves the uploaded file to baseDir and create the thumbnail
     *
     * @return $this
     */
    public function upload()
    {
        $this->file->move($this->baseDir,$this->name);

        $this->makeThumbnail();

        return $this;
    }

    /**
     *
     */
    private function makeThumbnail()
    {
        Image::make($this->baseDir.'/'.$this->fileName())
            ->fit(200)
            ->save($this->baseDir.'/'.$this->thumbName());
    }
}
