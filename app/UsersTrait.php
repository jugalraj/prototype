<?php

namespace App;

trait UsersTrait
{
    /**
     * Function : Groups
     * Function for retrieving associated groups
     */
    public function groups()
    {
        return $this->belongsToMany(Group::class,'acl_group_user');
    }

    /**
     * Function : getGroups
     * Function for returning all groups
     */
    public function getGroups()
    {
        $groups = $this->groups;

        $allGroup = [];

        foreach($groups as $group) {
            $allGroup[$group->id] = $group->name;
        }

        return $allGroup;
    }

    /**
     * Function : assignGroup
     * Function for assigning user to group
     * @param Group $group
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function assignGroup(Group $group)
    {
        return $this->groups()->save($group);
    }

    /**
     * @param $role
     * @return bool
     * @internal param $Role
     * @internal param Group $group
     */
    public function hasRole($role)
    {
        if(is_string($role)) {
            if($this->hasRoleAccess($role)) {
                return true;
            }
        }

        else {
            $groups = $this->groups;
            foreach ($groups as $group) {
                return $group->checkRole($role);
            }

            return false;
        }
    }

    /**
     * Function: hasRoleAccess
     * This will retrieve groups of the user and
     * call getRole method in Roles to get role
     *
     * @param $name
     * @return bool
     * @internal param Role $id
     */
    private function hasRoleAccess($name)
    {
        //get all associated groups with user
        $groups = $this->groups;

        //extract role_id from group and return it
        foreach($groups as $group) {
            return $group->groupHasRole($name);
        }
    }

    /**
     * Function : getUserRoles
     * Function for extracting all roles associated with user
     *
     * @return role
     */
    public function getUserRoles()
    {
        $groups = $this->groups;

        //extract role_id from group and return it
        foreach($groups as $group) {
            echo $group->name;
            $role[] = $group->getRoles();
        }

        dd($role);
    }
}